# Changelog
All notable changes to this project will be documented in this file.

## Revision 3.1
### Changed
- Replace RS-232 connector male version

## Revision 3.0
### Changed
- Change MDB interface to 3.3V level
- Add Level Shifter on RS232 interface
- Change pins for MDB interface (RX -> GPIO13, TX -> GPIO12, Sniffing -> GPIO5)
- Change pins for RS-232 interface (RX -> GPIO15, TX -> GPIO14)

## Revision 2.0
### Added
- Add MDB Sniffing for slave -> master communication (GPIO22)
- Add spare pin header to keep the jumper save when not in use.

### Changed
- Change MDB slave communication pins (RX -> GPIO27, TX -> GPIO17)
- Change RS-232 communication pins (RX -> GPIO6, TX -> GPIO5)

## Revision 1.0
### Added
- Minimal MDB interface (RX -> GPIO4, TX -> GPIO17)
- RS-232 interface using ADM3251EARWZ (RX -> GPIO14, TX -> GPIO13)
- Buck-Converter providing 5V/max. 3A
