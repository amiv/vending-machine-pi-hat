# Vending Machine Pi Hat

This hardware project aims to provide an easy and reliable way to use a Raspberry Pi as a cashless device over MDB (Multi-drop Bus) or RS-232.

## Specifications (Revision 3.x)

### Power Supply

| Input     | Output          |
|-----------|-----------------|
| 15-42V DC | 5V DC / max. 3A |

The system can be powered by the hat over MDB, RS-232 (CSS Maschine-Interface) or with the screw terminals.
It is also possible to power everything over 5V of the Raspberry Pi (Micro-USB connector).

### Communication Interface

|           | MDB            | RS-232             |
|-----------|----------------|--------------------|
| RX        | GPIO13 [RXD5]  | GPIO15 [RXD0/RXD1] |
| TX        | GPIO12 [TXD5]  | GPIO15 [TXD0/TXD1] |
| Sniffing _(Slave->Master)_ | GPIO5 [RXD3] |     |

## Specifications (Revision 2.0)

### Power Supply

| Input     | Output          |
|-----------|-----------------|
| 15-42V DC | 5V DC / max. 3A |

The system can be powered by the hat over MDB, RS-232 (CSS Maschine-Interface) or with the screw terminals.
It is also possible to power everything over 5V of the Raspberry Pi (Micro-USB connector).

### Communication Interface

|           | MDB    | RS-232  |
|-----------|--------|---------|
| RX        | GPIO27 | GPIO6   |
| TX        | GPIO17 | GPIO5   |
| Sniffing _(Slave->Master)_ | GPIO22 | -    |


## Changelog

For a detailed changelog on revisions, see [CHANGELOG](CHANGELOG.md).
